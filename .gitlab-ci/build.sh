#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-0.0.0+0}"
version="${tag%%+*}"
architecture="${ARCH:-amd64}"
tmpdir="$(mktemp -d)"
installdir="$(mktemp -d)"

case "${architecture}" in
    "amd64")
        aws_cli_architecture="x86_64"
        ;;
    "arm64")
        aws_cli_architecture="aarch64"
        dpkg --add-architecture arm64
        apt-get update
        apt-get -y install \
            qemu-user-binfmt \
            libc6:arm64 \
            zlib1g:arm64
        ;;
esac

apt-get update
apt-get -qqy install curl unzip

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.37.1}"
    curl -sfLo "${tmpdir}/nfpm.deb" "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${nfpm_architecture:-amd64}.deb"
    apt-get -qqy install "${tmpdir}/nfpm.deb"
fi

if [ ! -f "/tmp/${version}.zip" ]; then
    curl -sfLo "/tmp/${version}.zip" "https://awscli.amazonaws.com/awscli-exe-linux-${aws_cli_architecture}-${version}.zip"
fi
unzip "/tmp/${version}.zip" -d "${tmpdir}"
"${tmpdir}"/aws/install --install-dir "${installdir}"
echo 'exec /opt/aws-cli/bin/aws $*' > "${tmpdir}"/_aws

cat >"${tmpdir}/nfpm.yaml" <<EOF
name: aws-cli
arch: ${architecture}
version: ${tag}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: AWS Command Line Interface
homepage: https://docs.aws.amazon.com/cli/
depends:
  - groff
  - less
contents:
  - src: ${installdir}/v2/${version}/
    dst: /opt/aws-cli/
    type: tree
  - src: /opt/aws-cli/bin/aws
    dst: /usr/bin/aws
    type: symlink
  - src: /opt/aws-cli/bin/aws_completer
    dst: /usr/bin/aws_completer
    type: symlink
EOF
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
nfpm package --config "${tmpdir}/nfpm.yaml" --packager rpm
