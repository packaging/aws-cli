# [aws-cli](https://docs.aws.amazon.com/cli/) DEB/RPM Packages

Packages are created using [nfpm](https://nfpm.goreleaser.com/) by pushing released tags and repo is created using [Gitlabs static pages](https://morph027.gitlab.io/blog/repo-hosting-using-gitlab-pages/).

## DEB

### Add repo signing key to apt

```
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-aws-cli.asc https://packaging.gitlab.io/aws-cli/deb/gpg.key
```

### Add repo to apt

```
echo "deb https://packaging.gitlab.io/aws-cli/deb aws-cli main" | sudo tee /etc/apt/sources.list.d/morph027-aws-cli.list
```

### Install

```
sudo apt-get update
sudo apt-get install aws-cli aws-cli-keyring
```

## RPM

### Add repo to yum/dnf

```bash
cat >/etc/yum.repos.d/morph027-aws-cli.repo <<EOF
[aws-cli]
name=morph027-aws-cli
baseurl=https://packaging.gitlab.io/aws-cli/rpm/$basearch
enabled=1
gpgkey=https://packaging.gitlab.io/aws-cli/rpm/gpg.key
gpgcheck=1
repo_gpgcheck=1
EOF
```

### Install

```
sudo dnf install aws-cli
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50aws-cli <<EOF
Unattended-Upgrade::Allowed-Origins {
	"morph027:aws-cli";
};
EOF
```
